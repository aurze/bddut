# bddut

Unit test library for rust with a bdd flavor.

## example

### Test
```rust
unit_test! {
    when: is_balanced;
    given: "a balanced set of parantesis" s="()()";
    then: "it return balanced"
        true;
        
    given: "an unbalanced set of parantesis"
        s="())";
    then: "it return unbalanced"
        false;
}
```

### Reslt
```bash
running 3 tests
test tests::test_is_balanced_itreturnbalanced ... FAILED
test tests::test_is_balanced_itreturnunbalanced ... FAILED
test tests::test_is_equals_itreturnsmaller ... FAILED

failures:

---- tests::test_is_balanced_itreturnbalanced stdout ----
thread 'tests::test_is_balanced_itreturnbalanced' panicked at '
        Given : input is a balanced set of parantesis
        When  : calling is_balanced
        Then  : it return balanced (true)
        But   : actual was false
', src/lib.rs:56:5
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace

---- tests::test_is_balanced_itreturnunbalanced stdout ----
thread 'tests::test_is_balanced_itreturnunbalanced' panicked at '
        Given : input is an unbalanced set of parantesis
        When  : calling is_balanced
        Then  : it return unbalanced (false)
        But   : actual was true
', src/lib.rs:56:5

---- tests::test_is_equals_itreturnsmaller stdout ----
thread 'tests::test_is_equals_itreturnsmaller' panicked at '
        Given : input is x is smaller than y
        When  : calling is_equals
        Then  : it return smaller (Ok(- 1))
        But   : actual was Ok(1)
', src/lib.rs:69:5


failures:
    tests::test_is_balanced_itreturnbalanced
    tests::test_is_balanced_itreturnunbalanced
    tests::test_is_equals_itreturnsmaller

test result: FAILED. 0 passed; 3 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

error: test failed, to rerun pass `--lib`
```
