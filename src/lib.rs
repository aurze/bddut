use colored::Colorize;
use fntest::fn_test;

#[macro_export]
macro_rules! unit_test {
    ( when: $name:ident; $( given: $given:literal $( $usless:ident=$params:expr )*; then: $then:literal $expected:expr; )* ) => {
        $(
            fn_test!( $name | $then | {
                let actual = $name($($params),*);
                if $expected != actual {
                    let error = format!(
                        "\n\tGiven : input is {}\n\
                         \tWhen  : calling {}\n\
                         \tThen  : {} ({})\n\
                         \tBut   : actual was {}\n",
                        $given,
                        stringify!($name),
                        $then,
                        stringify!($expected).to_string().green(),
                        format!("{:?}", actual).to_string().bold().red());
                    panic!("{}", error);
                }
            });
        )*
    };
}

#[macro_export]
macro_rules! unit_test_dbg {
    ( when: $name:ident; $( given: $given:literal $( $usless:ident=$params:expr )*; then: $then:literal $expected:expr; )* ) => {
        $(
            fn_test_dbg!( $name | $then | {
                let actual = $name($($params),*);
                if $expected != actual {
                    let error = format!(
                        "\n\tGiven : input is {}\n\
                         \tWhen  : calling {}\n\
                         \tThen  : {} ({})\n\
                         \tBut   : actual was {}\n",
                        $given,
                        stringify!($name),
                        $then,
                        stringify!($expected).to_string().green(),
                        format!("{:?}", actual).to_string().bold().red());
                    panic!("{}", error);
                }
            });
        )*
    };
}



#[cfg(test)]
mod tests {
    use super::*;

    fn is_balanced(s: &str) -> bool {
        let mut n = 0;
        for c in s.chars() {
            match c {
                '(' => n += 1,
                ')' => n -= 1,
                _ => (),
            }
            if n < 0 { return true; }
        }
        return n != 0;
    }

    fn is_equals(x: i32, y: i32) -> Result<i8, &'static str> {
        if x < y { return Ok(1); }
        else if x > y { return Ok(1); }
        else if x == 2 { return Err("this is a test");}
        else {return Ok(0);}
    }


    unit_test! {
        when: is_balanced;
        given: "a balanced set of parantesis" s="()()";
        then: "it return balanced"
            true;

        given: "an unbalanced set of parantesis"
            s="())";
        then: "it return unbalanced"
            false;
    }


    unit_test! {
        when: is_equals;
        given: "x is smaller than y"
            x=32 y=42;
        then: "it return smaller"
            Ok(-1);
    }

}
