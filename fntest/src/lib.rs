extern crate proc_macro;
use proc_macro::TokenStream;

#[proc_macro]
pub fn fn_test(tokens: TokenStream) -> TokenStream {
    let binding = tokens.to_string();
    let mut token = binding.split('|');
    let f = format!("#[test]\nfn test_{}_{}() {}",
                    token.next().unwrap().trim(),
                    token.next().unwrap().trim().replace(&['(', ')', ',', '\"', '.', ';', ':', '\'', ' '], ""),
                    token.next().unwrap());

    f.parse().unwrap()
}

#[proc_macro]
pub fn fn_test_dbg(tokens: TokenStream) -> TokenStream {
    let binding = tokens.to_string();
    let mut token = binding.split('|');
    let f = format!("#[test]\nfn test_{}_{}() {}",
                    token.next().unwrap().trim(),
                    token.next().unwrap().trim().replace(&['(', ')', ',', '\"', '.', ';', ':', '\'', ' '], ""),
                    token.next().unwrap());

    panic!("{}", f);
}

